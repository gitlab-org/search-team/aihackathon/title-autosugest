# Title Autosugest

```bash
python -m venv .env
source .env/bin/activate
pip install -U pip setuptools wheel
pip install -U 'spacy[cuda-autodetect,transformers,lookups]'
python -m spacy download en_core_web_trf
```
src: <https://spacy.io/usage>

run this example:
```bash
python sumarizatorTest.py
```

current resutls:
```text
Original Document

Our Vue tests were slowly accumulating technical debt over time. Karma migration, recent @ vue/test-utils updates, our own researches on how to test components - all of these made our tests very different and sometimes very hard to maintain. Due to these issues it takes extremely long time to perform for example @ vue/test-utils upgrades, there is significant number of highly fragile tests in our codebase which dives too deep into implementation details, etc. This epic tracks our efforts to increase quality of frontend testing suite with significant focus on Vue related tests.
Total Length: 583


Summarized Document

Karma migration, recent @ vue/test-utils updates, our own researches on how to test components - all of these made our tests very different and sometimes very hard to maintain.
Total Length: 176
```

## Conclusion 

There seems to be several approaches to summarization. 
From what I tried this basic model is already very capable but if we train a pre-trained model for this specific task we can get much better results. 

## Resources

- <https://anubhav20057.medium.com/step-by-step-guide-abstractive-text-summarization-using-roberta-e93978234a90>
- <https://realpython.com/natural-language-processing-spacy-python/#shallow-parsing>
- <https://www.kaggle.com/code/sks27199/text-summarization-in-spacy-vs-gensim/notebook>
